from sklearn import svm
import spacy
import numpy as np

nlp = spacy.load('en_core_web_lg')
input_file = 'train.txt'
input_lines = open(input_file,'r').readlines()

inputs = [words.strip('.\n') for words in input_lines]

final_vector =np.zeros((len(inputs),6000))

for j,inp  in enumerate(inputs):
    word_vector = [tokens for tokens in nlp(inp)]
    vector = np.zeros((20,300))
    for i,token in enumerate  (word_vector):
        vector[i,:] = nlp.vocab[str(token)].vector
      
    final_vector[j] = vector.flatten()
    
y = [0,0,0,0,0,1,1,1,1,1,1]


clf = svm.SVC(kernel='linear', C = 1.0)

clf.fit(final_vector,y)

#####testing###############

input_file = 'test.txt'
input_lines = open(input_file,'r').readlines()
inputs = [words.strip('.\n') for words in input_lines]
final_vector =np.zeros((len(inputs),6000))

for j,inp  in enumerate(inputs):
    word_vector = [tokens for tokens in nlp(inp)]
    vector = np.zeros((20,300))
    for i,token in enumerate  (word_vector):
        vector[i,:] = nlp.vocab[str(token)].vector
      
    final_vector[j] = vector.flatten()
    
    
output = clf.predict(final_vector)
